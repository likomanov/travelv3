﻿using EmailService;
using EmailService.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace EmailApp.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class EmailSendController : Controller
    {
        private readonly IEmailSender _emaiSender;

        public EmailSendController(IEmailSender emailSender)
        {
            _emaiSender = emailSender;
        }

        [HttpGet]
        public async Task Get()
        {
            var rng = new Random();

            var message = new Message(new string[] { "iliyalikomanov@gmail.com", "shorty_m@abv.bg" }, "Test email async", "This is the content from my email.", null); // 1 - получател, 2 - subject, 3 - email body parameters, 4 - file attachments
            await _emaiSender.SendEmailAsync(message);
        }

        [HttpPost]
        public async Task Post()
        {
            var rng = new Random();

            var files = Request.Form.Files.Any() ? Request.Form.Files : new FormFileCollection();

            var message = new Message(new string[] { "iliyalikomanov@gmail.com", "shorty_m@abv.bg" }, "Test email with attachments", "This is the content from my email with attachments.", files); // 1 - получател, 2 - subject, 3 - email body parameters
            await _emaiSender.SendEmailAsync(message);
        }
    }
}
